/*******************************************************************************
 * asc2c, is a climate data preprocessor for the ech2o modeling package
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     asc2c is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     asc2c and ech2o are distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with asc2c.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta
 *******************************************************************************/

#include <fstream>
#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char* argv[]) {

	if(argc != 3){
		cout << endl << "CLIM2ASCII v0.1beta - Watershed Hydrology Lab - University of Montana" << endl << endl;
		cout << "USAGE: c2asc BinFile AsciiFile " << endl;
		cout << "WHERE: BinFile is a climate binary file with correct climate format (see documentation for format)" << endl;
		cout << "USAGE: AsciiFile is the name of the output file where climate information is dumped in ascii format " << endl;
		exit(EXIT_SUCCESS);

	}


		ifstream input;
		ofstream output;
		char comment[256];

		int numTimeSteps, numStations;
		float *TimeStep;
		int *Zns;
		float *data;
		unsigned int data_read = 0;


	   input.open(argv[1], ios::binary);
	   output.open(argv[2]);


	  if(!input.is_open() || !output.is_open())
	  {
	  	        cout << "Cannot open file " << (!input.is_open() ? argv[1] : argv[2]) << endl;
	  	        perror("FATAL ERROR");
	  	        return EXIT_FAILURE;
	  }

	  input.read(comment, sizeof(comment));
	  output << comment << endl;

	  //reads one int with the number of time steps
	  input.read((char *)&numTimeSteps, sizeof(int));
	  output << numTimeSteps << endl;

	  TimeStep = new float[numTimeSteps];

   	  //reads numTimeSteps floats into the TS TimeStep
	  input.read((char *)TimeStep, sizeof(float)*numTimeSteps);
	  for (int i = 0; i < numTimeSteps; i++)
		  output << TimeStep[i] << " ";
	  output << endl;

	  input.read((char *)&numStations, sizeof(int));
	  
	  output << numStations << endl;

	  Zns = new int[numStations];

	  input.read((char *)Zns, sizeof(int)*numStations); //read as many zones as there are zones in teh map
	  for (int i = 0; i < numStations; i++)
	  		  output << Zns[i] << " ";
	  output << endl;

	  data = new float[numStations]; //creates the array to hold the data
	  for (int i=0; i<numTimeSteps; i++){
	      input.read((char *)data, sizeof(float)*numStations); //reads data for all zones
	      for(int a=0; a < numStations; a++)
	    	  output << data[a] << " ";
	      output << endl;
	  }


	 if(TimeStep)
			delete[] TimeStep;
     if(data)
	        delete[] data;
	 if(Zns)
	     delete[] Zns;

      output.close();
      input.close();


	return EXIT_SUCCESS;
}





	//reads one int with the number of zones







