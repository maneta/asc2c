/*******************************************************************************
 * asc2c, is a climate data preprocessor for the ech2o modeling package
 * Copyright (c) 2016 Marco Maneta <marco.maneta@umontana.edu>
 *
 *     This file is part of ech2o, a hydrologic model developed at the
 *     University of Montana.
 *
 *     asc2c is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     asc2c and ech2o are distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with asc2c.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *    Marco Maneta
 *******************************************************************************/

#include <fstream>
#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char* argv[]) {

	if(argc != 3){
		cout << endl << "ASCII2CLIM v0.1beta - Watershed Hydrology Lab - University of Montana" << endl << endl;
		cout << "USAGE: asc2c AsciiFile BinFile" << endl;
		cout << "WHERE: AsciiFile is an ascii file wiht the correct climate format (see documentation for format)" << endl;
		cout << "USAGE: BinFile is the name of the output file with binary climate information to be used in ECH2O" << endl;
		exit(EXIT_SUCCESS);

	}


		ifstream input;
		ofstream output;
		char comment[256];
		for (unsigned int i = 0; i < sizeof(comment); i++)
			comment[i] = ' ';
		int numTimeSteps, numStations, StationID;
		float TimeStep, data;


	   input.open(argv[1]);
	   output.open(argv[2], ios::binary);


	  if(!input.is_open() || !output.is_open())
	  {
	  	        cout << "Cannot open file " << (!input.is_open() ? argv[1] : argv[2]) << endl;
	  	        perror("FATAL ERROR");
	  	        return EXIT_FAILURE;
	  }

	  input.getline(comment, 256);
	  output.write(comment,256);

	  input >> numTimeSteps;
	  output.write((char *)&numTimeSteps, sizeof(int));
	  for(int i = 0; i < numTimeSteps; i++)
	  {
	  	input>> TimeStep;
	  	output.write((char *)&TimeStep, sizeof(float));
	  }

	  input>>numStations;
	  output.write((char *)&numStations, sizeof(int));
	  for (int i = 0; i < numStations; i++)
	  {
		  input >> StationID;
		  output.write((char *)&StationID, sizeof(int));
	  }

	  for (int i = 0; i < numStations * numTimeSteps; i++)
	  {
	  	input >> data;
	  	output.write((char *)&data, sizeof(float));
	  }



	      //input.read((char *)&tstep, sizeof(int));
	   output.close();


	   input.close();


	return EXIT_SUCCESS;
}
