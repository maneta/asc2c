INSTALLATION
============

The following file contains information on how to install and compile the latest version of ECH2O.  


0. Overview

1 Making ``asc2c``
2. Contact


2 Making ``asc2c``
------------------

  * Open a command line terminal 
 
  * Change directory to your workspace and clone the latest version of the source files from the git repository:

   ::

   $ git clone https://maneta@bitbucket.org/maneta/asc2c.git

  * Change directory into the source folder and type ``make`` to make the code. 

    

4. Contact
----------

  If you need assistance compiling the source, contact marco.maneta@umontana.edu

    


