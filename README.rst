ASC2C, a climate data preprocessor for ECH2O
============================================

What is it?
-----------

  ASC2C is a small utility to convert climate information in ASCII format to binary format prior to ingestion in the ech2o model (http://hs.umt.edu/RegionalHydrologyLab/software/default.php)  

The Latest Version
------------------

  Details of the latest version (source and binaries) can be found on the ECH2O software page under http://hs.umt.edu/RegionalHydrologyLab/software/default.php.
   

Documentation
-------------

  The most
  up-to-date documentation can be found at
  http://hs.umt.edu/RegionalHydrologyLab/software/documentation-ech2o.php.
  

Installation of Binary Distributions
------------------------------------

  Please see the file called INSTALL.rst.
  
Compilation of source code
--------------------------
  
  Please see the file called INSTALL.rst.

Licensing
---------

  Please see the file called LICENSE.txt.

Bugs
____

  Should you encounter any bug, please file a ticket in https://bitbucket.org/maneta/asc2c/issues

Known Issues
------------

  Known issues can be found in https://bitbucket.org/maneta/asc2c/issues

Contacts
--------

  If you have any questions please, contact marco.maneta@umontana.edu.